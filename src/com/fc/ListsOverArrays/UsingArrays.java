package com.fc.ListsOverArrays;
import java.util.*;

public class UsingArrays {

    public void sloppyArrays(){
        // Arrays are covariant. Since Long is a subclass of object, no compilation error is thrown
        Object[] objArr = new Long[5];
        objArr[0] = "I am a string";

    }

}

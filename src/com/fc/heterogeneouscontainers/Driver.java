package com.fc.heterogeneouscontainers;

public class Driver {

    public static void main(String[] args) {
        Favourites f = new Favourites();
        f.putFavorite(String.class, "Java");
        f.putFavorite(Integer.class, 1);
        f.putFavorite(Class.class, Favourites.class);
        String favoriteString = f.getFavorite(String.class);
        int favoriteInteger = f.getFavorite(Integer.class);
        Class<?> favoriteClass = f.getFavorite(Class.class);
        System.out.printf("%s %x %s%n", favoriteString, favoriteInteger, favoriteClass.getName());
    }
}

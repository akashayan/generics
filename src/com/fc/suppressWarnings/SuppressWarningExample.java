package com.fc.suppressWarnings;

import java.util.ArrayList;
import java.util.Arrays;

public class SuppressWarningExample <T> {

    private int size = 16;
    private T[] elements = (T[])new Object[size];

    public <T> T[] toArray(T[] a) {

        /*if (a.length < size)
            return (T[]) Arrays.copyOf(elements,size, a.getClass()); // Warning as copyOf will return Object due to type erasure at runtime
        System.arraycopy(elements, 0, a, 0, size);
        if (a.length > size)
            a[size] = null;
        return a;*/

        if (a.length < size) {
// This cast is correct because the array we're creating
// is of the same type as the one passed in, which is T[].
            @SuppressWarnings("unchecked")
            T[] result = (T[]) Arrays.copyOf(elements, size, a.getClass()); // Adding local variable to  reduce scope of @SuppressWarnings
            return result;

        }
        return null;
    }
}


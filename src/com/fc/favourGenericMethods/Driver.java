package com.fc.favourGenericMethods;

import java.util.HashSet;
import java.util.Set;

public class Driver {

    public static void main(String args[]){
        Set s1 = new HashSet();
        Set s2 = new HashSet();
        s1.add("Hello");
        s2.add(1);

        WithoutGenerics.union(s1,s2);

        Set<String> s3 = new HashSet<>();
        Set<Integer> s4 = new HashSet<>();

        //WithGenerics.unionWithGenerics(s3,s4); // compilation error

    }
}

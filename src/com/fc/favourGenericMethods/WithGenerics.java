package com.fc.favourGenericMethods;

import java.util.HashSet;
import java.util.Set;

public class WithGenerics {

    public static <E> Set<E> unionWithGenerics(Set<E> s1, Set<E> s2) {
        Set<E> result = new HashSet<>(s1);
        result.addAll(s2);
        return result;
    }
}

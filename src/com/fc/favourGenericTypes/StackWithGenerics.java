package com.fc.favourGenericTypes;

import java.util.Arrays;

public class StackWithGenerics<E> {

    private E[] elements;
    private int size = 0;
    private static final int DEFAULT_INITIAL_CAPACITY = 16;

    @SuppressWarnings("unchecked") // Compiling gives a warning without suppressWarnings. After convincing yourself of the type safety
    public StackWithGenerics() {

        elements = (E[]) new Object[DEFAULT_INITIAL_CAPACITY];
    }

    public void push(E e) {
        ensureCapacity();
        elements[size++] = e;
    }
    public E pop() {
        if (size == 0)
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }

        E result = elements[--size];
        elements[size] = null; // Eliminate obsolete reference
        return result;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    private void ensureCapacity() {
        if (elements.length == size)
            elements = Arrays.copyOf(elements, 2 * size
                    + 1);
    }


    public void pushAll(Iterable<E> src) {
        for (E e : src)
            push(e);
    }
}

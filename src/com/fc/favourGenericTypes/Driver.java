package com.fc.favourGenericTypes;

public class Driver {

    public static void main(String args[]){
 /*       Stack stack = new Stack();
        Integer integer = new Integer(5);
        String string = "hey";

        stack.push(integer);
        stack.push(string);

        while(!stack.isEmpty()){
            System.out.println("Element : "+(Integer)stack.pop());
        }*/

        Integer integer = new Integer(5);
        String string = "hey";
        //Type-safe
       StackWithGenerics<Integer> stackWithGenerics = new StackWithGenerics<>();
        stackWithGenerics.push(integer);
        //stackWithGenerics.push(string);

        while(!stackWithGenerics.isEmpty()){
            System.out.println("Element new : "+(Integer)stackWithGenerics.pop());
        }


    }
}

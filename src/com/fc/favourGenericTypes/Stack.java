package com.fc.favourGenericTypes;

import java.util.Arrays;


/*        As it stands, the client has to cast objects
        that are popped off the stack,
        and those casts might fail at runtime*/

public class Stack {

    private Object[] elements;
    private int size = 0;
    private static final int DEFAULT_INITIAL_CAPACITY = 16;

    public Stack() {
        elements = new Object[DEFAULT_INITIAL_CAPACITY];
    }


    public void push(Object e) {
        ensureCapacity();
        elements[size++] = e;
    }


    public Object pop() {
        if (size == 0)
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }

        Object result = elements[--size];
        elements[size] = null; // Eliminate obsolete reference
        return result;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    private void ensureCapacity() {
        if (elements.length == size)
            elements = Arrays.copyOf(elements, 2 * size
                    + 1);
    }
}

package com.fc.genericsAndVarargs;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class Driver {

    public static void main(String args[]){
        Dangerous obj = new Dangerous();
        Pair<String,String> pair1 = new Pair<>("","");
        Pair<String,String> pair2 = new Pair<>("","");
        //obj.method(pair1,pair2);

        List<String> list = new ArrayList<>();
        list.add("");
        obj.dangerous(list);

    }
}

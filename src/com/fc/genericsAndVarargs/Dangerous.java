package com.fc.genericsAndVarargs;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class Dangerous {

     void dangerous(List<String>... stringLists) {
        List<Integer> intList = new ArrayList<>();
        intList.add(42);

        Object[] objects = stringLists;
        objects[0] = intList; // Heap pollution
        String s = stringLists[0].get(0); // ClassCastException warning
                                          // as at runtime <String> is erased and stringLists becomes a raw type
    }

    Pair<String,String>[] method(Pair<String,String>... lists) {
        Object[] objs = lists;
        objs[0] = new Pair<String,String>("x","y");
        objs[1] = new Pair<Long,Long>(0L,0L);  // corruption !!!
        System.out.println(lists[0].getKey());
        System.out.println(lists[1].getKey());
        return lists;
    }
}

package com.fc.difference;

import java.util.*;

public class DifferenceInParameters {

    public static void main(String args[]){

        List<String> list = new ArrayList<>();
        list.add("KLICKPAY");
        list.add("UPI");

        //legalPayments(list);

        /*
            There are sub-typing rules for generics,
            and List<String> is a subtype of raw type List,
            but not of the parameterized type List<Object>
        * */

        //illegalPayments(list);

        unSafeAdd(list, Integer.valueOf(1));
        // ClassCastException
        String s = list.get(2);

        safeAdd(list,"hello");

    }

    private static void legalPayments(List list){

        System.out.println("Legal");


    }

    private static void illegalPayments(List<Object> list){

        System.out.println("Illegal");
    }

    private static void unSafeAdd(List list, Object o){
        list.add(o);
        System.out.println("illegal");
    }

    private static void safeAdd(List<?> list, Object o){
        /*
            Wild cards are type safe and if you try to add an object,
            it will give you a compilation error.you can't put any
            element other than null into a Collection<?>. Although this
            restricts your ability to add any element but the compiler has done its job.
        * */
        //list.add(o);
        list.add(null);

    }
}

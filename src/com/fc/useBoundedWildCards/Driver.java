package com.fc.useBoundedWildCards;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Driver {

    public static void main(String args[]){

        WithoutBoundedWildCards<Number> stack = new WithoutBoundedWildCards<>();
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);

        List<Object> objList = new ArrayList<>();

        //stack.pushAll(list); // compilation error as list is of typ Integer and pushAll is of type Number
        //stack.popAll(objList);


        //WithBoundedWildCards<Number> newStack = new WithBoundedWildCards<>();

        WithBoundedWildCards<Number> newStack = new WithBoundedWildCards<>();

        newStack.pushAll(list);
        newStack.popAll(objList);
    }
}

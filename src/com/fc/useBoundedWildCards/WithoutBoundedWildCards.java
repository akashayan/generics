package com.fc.useBoundedWildCards;

import java.util.Arrays;
import java.util.List;

public class WithoutBoundedWildCards<E> {

    private E[] elements;
    private int size = 0;
    private static final int DEFAULT_INITIAL_CAPACITY = 16;

    @SuppressWarnings("unchecked")
    public WithoutBoundedWildCards() {
        elements = (E[]) new Object[DEFAULT_INITIAL_CAPACITY];
    }

    public void push(E e) {
        ensureCapacity();
        elements[size++] = e;
    }
    public E pop() {
        if (size == 0)
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }

        E result = elements[--size];
        elements[size] = null; // Eliminate obsolete reference
        return result;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    private void ensureCapacity() {
        if (elements.length == size)
            elements = Arrays.copyOf(elements, 2 * size
                    + 1);
    }

    public void pushAll(List<E> src) {
        for (E e : src)
            push(e);
    }

    public void popAll(List<E> dst) {
        while (!isEmpty())
            dst.add(pop());
    }


}

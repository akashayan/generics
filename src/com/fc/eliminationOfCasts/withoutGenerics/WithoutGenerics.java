package com.fc.eliminationOfCasts.withoutGenerics;

import java.util.ArrayList;
import java.util.List;



public class WithoutGenerics {

    public static void main(String args[]){
        /*
            Without generics, we have to explicitly typecast the return value.

      */


        /*
            We can add an Object subclass and type-checking will not
            be performed at compile time and the code is prone to
            ClassCastException.
        */

        List anIntegerList = new ArrayList(); // Raw type of generic List<E>
        Integer integer = new Integer(1);
        anIntegerList.add(integer);

        String string = new String("Hello");
        anIntegerList.add(string);


        sum(anIntegerList);

    }

    private static void sum(List anIntegerList) {
        Integer a = (Integer)anIntegerList.get(0);

        // ClassCastException here
        Integer b = (Integer)anIntegerList.get(1);

        Integer sum = new Integer(a.intValue()+b.intValue());

        System.out.println("sum : "+sum);

    }


}

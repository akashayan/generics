package com.fc.eliminationOfCasts.withGenerics;

import java.util.*;

public class WithGenerics {

    public static void main(String args[]){
        List<Integer> anIntegerList = new ArrayList<Integer>();
        Integer integer = new Integer(1);
        Object string = new String("Hello");

        anIntegerList.add(integer);

        //Won't compile due to strong type-checking because of generics.
        //anIntegerList.add(string);

        sum(anIntegerList);
    }
    private static void sum(List<Integer> anIntegerList) {
        Integer a = anIntegerList.get(0);
        Integer b = anIntegerList.get(1);

        Integer sum = new Integer(a.intValue()+b.intValue());

        System.out.println("sum : "+sum);

    }

}
